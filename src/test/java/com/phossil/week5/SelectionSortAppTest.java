package com.phossil.week5;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SelectionSortAppTest {
    @Test
    public void shouldFindMinIndexTestCast1() {
        int arr[] = { 5, 4, 3, 2, 1 };
        int pos = 0;
        int minIndex = SelectionSortApp.findMinIndex(arr, pos);
        assertEquals(4, minIndex);
    }

    @Test
    public void shouldFindMinIndexTestCast2() {
        int arr[] = { 1, 4, 3, 2, 5 };
        int pos = 1;
        int minIndex = SelectionSortApp.findMinIndex(arr, pos);
        assertEquals(3, minIndex);
    }

    @Test
    public void shouldFindMinIndexTestCast3() {
        int arr[] = { 1, 2, 3, 4, 5 };
        int pos = 2;
        int minIndex = SelectionSortApp.findMinIndex(arr, pos);
        assertEquals(2, minIndex);
    }

    @Test
    public void shouldFindMinIndexTestCast4() {
        int arr[] = { 1, 1, 1, 1, 1, 0, 1, 1 };
        int pos = 0;
        int minIndex = SelectionSortApp.findMinIndex(arr, pos);
        assertEquals(5, minIndex);
    }

    @Test
    public void shouldSwapTestCast1() {
        int arr[] = { 5, 4, 3, 2, 1 };
        int expected[] = { 1, 4, 3, 2, 5 };
        int first = 0;
        int second = 4;
        SelectionSortApp.swap(arr, first, second);
        assertArrayEquals(expected, arr);
    }

    @Test
    public void shouldSelectionSortTestCase1() {
        int arr[] = { 5, 4, 3, 2, 1 };
        int sortedArr[] = { 1, 2, 3, 4, 5 };
        SelectionSortApp.selectionsort(arr);
        assertArrayEquals(sortedArr, arr);
    }

    @Test
    public void shouldSelectionSortTestCase2() {
        int arr[] = { 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 };
        int sortedArr[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        SelectionSortApp.selectionsort(arr);
        assertArrayEquals(sortedArr, arr);
    }

    @Test
    public void shouldSelectionSortTestCase3() {
        int arr[] = { 6, 9, 8, 7, 10, 5, 4, 3, 2, 1 };
        int sortedArr[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        SelectionSortApp.selectionsort(arr);
        assertArrayEquals(sortedArr, arr);
    }

}
